#!/usr/bin/env bash

set -ex

if [ ! -f $CI_PROJECT_DIR/email.txt ]; then
    exit
fi

cat <<EOF> ~/.msmtprc
defaults
auth           on
tls            on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile        ~/.msmtp.log
account        linaro
host           smtp.gmail.com
port           587
from           $USER_EMAIL
user           $USER_EMAIL
password       $SMTP_PASS
account default : linaro
EOF

chmod 600 ~/.msmtprc

cat<<EOF> $CI_PROJECT_DIR/header.txt
To: $USER_EMAIL
From: $USER_EMAIL
Subject: [$CI_PROJECT_NAME] [CI-NOTIFY]  [$CI_JOB_NAME] [$CONFIGS_PIPELINE]: Pipeline $CI_PIPELINE_ID: ${CI_JOB_STATUS}!
EOF

cat $CI_PROJECT_DIR/header.txt $CI_PROJECT_DIR/email.txt | msmtp $USER_EMAIL
