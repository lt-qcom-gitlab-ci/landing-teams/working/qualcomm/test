#!/usr/bin/env bash

set -ex

echo 'success' > $CI_PROJECT_DIR/CI_JOB_STATUS

cat<<EOF>$CI_PROJECT_DIR/email.txt

Build URL: ${CI_PIPELINE_URL}

Automerge repo/branch: ${AUTOMERGE_REPO_URL} ${AUTOMERGE_BRANCH}

Kernel upstream repository: ${KERNEL_REPO_URL}
Integration repo/branch: ${INTEGRATION_REPO_URL} ${INTEGRATION_BRANCH}
Kernel CI repo/branch: ${KERNEL_CI_REPO_URL} ${KERNEL_CI_BRANCH}

${AUTOMERGE_BRANCH_FAILED}
Automerge configuration:
${AUTOMERGE_CONFIG}

DTBS warnings:
EOF
cat $CI_PROJECT_DIR/email.txt

cat $CI_PROJECT_DIR/dtbs_warnings.log >> $CI_PROJECT_DIR/email.txt
echo 2
cat $CI_PROJECT_DIR/email.txt

cat <<EOF>>$CI_PROJECT_DIR/email.txt

${PATCH_COUNT} patches in the integration branch
QCLT Wall Of Fame:
EOF
echo 3
cat $CI_PROJECT_DIR/email.txt

cat $CI_PROJECT_DIR/wall_of_fame.log >> $CI_PROJECT_DIR/email.txt
cat $CI_PROJECT_DIR/email.txt
